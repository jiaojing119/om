package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.PrizeRecord;
import com.zero2oneit.mall.common.query.member.PrizeRecordQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.dto.PrizeDTO;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
public interface PrizeRecordService extends IService<PrizeRecord> {

    BoostrapDataGrid pageList(PrizeRecordQueryObject qo);

    /**
     * 会员抽奖
     * @param prizeDTO
     * @return
     */
    R draw(PrizeDTO prizeDTO);

}

