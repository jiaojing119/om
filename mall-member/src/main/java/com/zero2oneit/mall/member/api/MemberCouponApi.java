package com.zero2oneit.mall.member.api;

import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.service.MemberCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 优惠券管理
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/4/18
 */
@RestController
@RequestMapping("/api/auth/member/coupon")
public class MemberCouponApi {

    @Autowired
    private MemberCouponService couponService;

    /**
     * 加载会员优惠券列表
     * @param qo 参数
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody MemberCouponQueryObject qo){
        BoostrapDataGrid page = couponService.pageList(qo);
        return R.ok("加载成功", page);
    }

    /**
     * 加载优惠券
     * @return
     */
    @RequestMapping("/load")
    public R load(@RequestBody MemberCouponQueryObject qo){
        return couponService.load(qo);
    }

    /**
     * 领取优惠券
     * @return
     */
    @RequestMapping("/receive")
    public R receive(@RequestBody MemberCouponQueryObject qo){
        return couponService.receive(qo);
    }

}
