/**  
 * 缓存数据优化  
 * import cache from '@/common/js/utils/cache'  
 * 使用方法 【  
 *     一、设置缓存  
 *         string    cache.put('k', 'string你好啊');  
 *         json      cache.put('k', { "b": "3" }, 2);  
 *         array     cache.put('k', [1, 2, 3]);  
 *         boolean   cache.put('k', true);  
 *     二、读取缓存  
 *         默认值    cache.get('k')  
 *         string    cache.get('k', '你好')  
 *         json      cache.get('k', { "a": "1" })  
 *     三、移除/清理    
 *         移除: cache.remove('k');  
 *         清理：cache.clear();   
 * 】  
 * @type {String}  
 */  
export default class cache {
  static postfix = '_om';
  constructor() {
	  // 缓存前缀 
	  // 这里在构造函数中进行this绑定
  }
/**  
 * 设置缓存   
 * @param  {[type]} k [键名]  
 * @param  {[type]} v [键值]  
 * @param  {[type]} t [时间、单位秒]  
 */  
static put(k, v, t) {  
    uni.setStorageSync(k, v);
    let seconds = parseInt(t);  
    if (seconds > 0) {  
        let timestamp = Date.parse(new Date());  
        timestamp = timestamp / 1000 + seconds;  
        uni.setStorageSync(k + this.postfix, timestamp + "")  
    } else {  
        uni.removeStorageSync(k + this.postfix)
    }  
}  

/**  
 * 获取缓存   
 * @param  {[type]} k   [键名]  
 * @param  {[type]} def [获取为空时默认]  
 */  
static get(k, def) {
    let deadtime = parseInt(uni.getStorageSync(k + this.postfix));
    if (deadtime) {  
        if (parseInt(deadtime) < Date.parse(new Date()) / 1000) {
			// 设置缓存时间结束
            if (def) {  
                return def;  
            } else {
				uni.removeStorageSync(k);
				uni.removeStorageSync(k + this.postfix);
                return false;  
            }  
        }
    }  
    let res = uni.getStorageSync(k);  
    if (res) {  
        return res;  
    } else {  
        if (def == undefined  || def == "") {  
            def = false;   
        }  
        return def;  
    }  
}  

/**
 * 根据key删除缓存
 * @param {Object} k [键名]  
 */
static remove(k) {  
    uni.removeStorageSync(k);  
    uni.removeStorageSync(k + this.postfix);  
}


/*设置缓存指定长度的缓存   
 * @param  {[type]} key [键名]  
 * @param  {[type]} nValue [键值]  
 * @param  {[type]} length [长度] 
 */
static  putByLimit(key, nValue,fn, length=10) {  
   uni.getStorage({
   	key,
   	success: res => {
   		var OldKeys = JSON.parse(res.data);
   		var findIndex = OldKeys.indexOf(nValue);
   		if (findIndex == -1) {
   			OldKeys.unshift(nValue);
   		} else {
   			OldKeys.splice(findIndex, 1);
   			OldKeys.unshift(nValue);
   		}
   		//最多length个纪录
   		OldKeys.length > length && OldKeys.pop();
   		uni.setStorage({
   			key,
   			data: JSON.stringify(OldKeys)
   		});
		fn(OldKeys);
   		// return OldKeys; //返回缓存内容
   	},
   	fail: e => {
   		var OldKeys = [nValue];
   		uni.setStorage({
   			key,
   			data: JSON.stringify(OldKeys)
   		});
   		fn(OldKeys); //返回缓存内容
   	}
   });
}    

}