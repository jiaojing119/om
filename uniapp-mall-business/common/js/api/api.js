import Request from '@/common/js/luch-request/index.js'
import base from '@/common/js/api/api.config.js'
import store from '@/store/index.js'

//获取缓存中的token
const getTokenStorage = () => {
	let token = '';
	try {
		token = store.state.userInfo? store.state.userInfo.token :''
	} catch (e) {
		//TODO handle the exception
	}
	return token
};

// 创建请求实例
const http = new Request();

/**
 * 设置全局配置：
 * baseURL：请求服务地址
 * header：请求头	   
 */
http.setConfig((config) => { 
	config.baseURL = base.url;
	config.header = {
	  ...config.header,
	  token: store.state.userInfo? store.state.userInfo.token :'',
	  authType: "true"
	};
	config.custom = {
	  auth: false, // 是否传token
	    // loading: false // 是否使用loading
	};
	return config
});

/**
 * 请求之前拦截器。可以使用async await 做异步操作
 */
http.interceptors.request.use((config) => {
	//请求之前业务处理 
	// 未登录拦截
	if(!store.state.userInfo && config.header.authType == 'true'){
		uni.showToast({ title: '您还没登录，请先登录', icon: "none"});
		setTimeout(function() {
			uni.navigateTo({
				url: "/pages/auth/btnAuth/btnAuth"  //改成业务所需跳转页面
			})
		},1500);
		return
	}
	config.header.token = store.state.userInfo? store.state.userInfo.token :'';
	return config
}, (config) => {
	return Promise.reject(config)
});
 
/**
 * 请求响应拦截器。必须使用异步函数，注意
 */
http.interceptors.response.use(async (response) => { 
	// 请求响应业务处理
	if (response.data.code == -1) { // 登录已过期
		uni.showToast({
			title: response.data.msg,
			icon: 'none'
		});
		store.commit('getUserInfo','');
		setTimeout(function() {
			uni.navigateTo({
				url: "/pages/auth/btnAuth/btnAuth", //改成业务所需跳转页面
			})
		}, 1000);
		return response.data
	}
	return response.data
}, (response) => { 
	// 请求异常时做点什么。可以使用async await 做异步操作
	return Promise.reject(response.data)
});

// 可以自定义局部变量，在自定义变量里面增加拦截业务处理，如日志 权限 通知等
export { http }